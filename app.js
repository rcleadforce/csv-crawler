var path = require('path');
var fs   = require('fs');
var detector = require('detect-csv');
var csv = require('csv-parser');
/* const mysql = require('mysql2');
 const mysqlconn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    database: 'test'
  }); */

//
// ----------- START -----------
//
const country = 'IT'; 
var allCSV = findFilesInDir('./Dropbox/'+country,'.csv');
const logger = fs.createWriteStream(country+'-result-1.csv');
logger.write('"country"|"path"|"client"|"email"|"ip"|"timestamp"\r\n');
allCSV.forEach(function(path) {
    //We keep the delivered CSVs in the Delivery folder, can be one or more per folder
    try{
        if(path.includes('Delivery')) {
            fs.readFile(path, "utf8", function(err, data) {
                var csv = detector(data);
                parseFile(path, csv.delimiter, csv.newline);
            });
        }
    }
    catch(err){
        console.log('Error with: '+path+' Message: '+err);
    }
});
 
//
// ----------- END -----------
//

/**
 * Parse CSV to JS array with csv-parser 
 * @param  {String} filepath    Path relative to this file
 * @param  {String} delimeter   specify cell separator
 * @param  {String} lineend     specify a newline characte
 */
function parseFile(filepath, delimeter, lineend){
    var array = [];
    fs.createReadStream(filepath)
    .pipe(csv({
        raw: false,     // do not decode to utf-8 strings
        separator: delimeter, // specify optional cell separator
        newline: lineend // specify a newline characte
      }))
    .on('data', function (data) {
        array.push(data);
    })
    .on('end', function(){
        var fieldmapping = getFields(array[0]); //I take the first row to check the columns
        var client = filepath.split('\\')[2]; //Get Client from Path, adjust according to the current path!
        var deliveryId = saveClientDelivery(filepath, client); 
        for (var row in array){   
            saveDeliveredData(filepath, client, deliveryId, array[row][fieldmapping.email], array[row][fieldmapping.ip], array[row][fieldmapping.timestamp]);
        }
    });
}

function saveClientDelivery(filepath, client){
    //Save to db here
    // console.log(filepath+', '+path.parse(filepath).base+', '+client);
    // logger.write(filepath+'|'+path.parse(filepath).base+'|'+client+'\r\n');
    //Return foreign key
    return 1;
}

function saveDeliveredData(filepath, client, deliveryid, email, ip, timestamp){
    //Save to DB here
    // console.log(deliveryid+', '+email+', '+ip+', '+timestamp);
    logger.write('"'+country+'"|"'+filepath+'"|"'+client+'"|"'+email+'"|"'+ip+'"|"'+timestamp+'"\r\n');
}

/**
 * Finds teh key fields in the array
 * @param  {array} array    Parsed CSV file
 * @return {array} result   Key columns for the parse
 */
function getFields(array){
    var result = {
        email: null,
        ip: null,
        timestamp: null
    }; 
    for (var row in array) {
        if (array.hasOwnProperty(row)) {
            //if it has @ char then I cosider it an email
            if(array[row].includes('@')){ 
                result.email = row; 
            }
            //Check for IPv4 address pattern
            if(array[row].match('^(([0-9][0-9]?|[0-1][0-9][0-9]|[2][0-4][0-9]|[2][5][0-5])\\.){3}([0-9][0-9]?|[0-1][0-9][0-9]|[2][0-4][0-9]|[2][5][0-5])')){ 
                result.ip = row;
            }
            //If the string contains one of these years (possible birtdates excluded)
            if(array[row].match('(2018|2017|2016|2015|2014|2013|2012|2011)'))
            {
                try{
                    var dateparse = new Date(array[row]); //Parse to date then check if it's valid
                    if(dateparse instanceof Date){
                        result.timestamp = row;
                    }
                }
                catch(e){
                    //skip and ignore
                }
            }
        }
   }
   return result;
}

/**
 * Find all files recursively in specific folder with specific extension, e.g:
 * findFilesInDir('./project/src', '.csv') ==> ['./project/src/a.csv','./project/src/build/file.csv']
 * @param  {String} startPath    Path relative to this file or other file which requires this files
 * @param  {String} filter       Extension name, e.g: '.csv'
 * @return {Array}               Result files with path string in an array
 */
function findFilesInDir(startPath,filter){

    var results = [];

    if (!fs.existsSync(startPath)){
        console.log("no dir ",startPath);
        return;
    }

    var files=fs.readdirSync(startPath);
    for(var i=0;i<files.length;i++){
        var filename=path.join(startPath,files[i]);
        var stat = fs.lstatSync(filename);
        if (stat.isDirectory()){
            results = results.concat(findFilesInDir(filename,filter)); //recurse
        }
        else if (filename.indexOf(filter)>=0) {
            console.log('-- found: ',filename);
            results.push(filename);
        }
    }
    return results;
}
